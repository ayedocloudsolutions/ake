## [1.5.5](https://gitlab.com/ayedocloudsolutions/ake/compare/v1.5.4...v1.5.5) (2021-08-12)


### Bug Fixes

* removed ssh host key check ([0774851](https://gitlab.com/ayedocloudsolutions/ake/commit/0774851d37ac8377337be01ed960425421ee66bf))

## [1.5.4](https://gitlab.com/ayedocloudsolutions/ake/compare/v1.5.3...v1.5.4) (2021-08-12)


### Bug Fixes

* config access method ([76e2ec9](https://gitlab.com/ayedocloudsolutions/ake/commit/76e2ec94264e93e638f626fbf07be938f611c6eb))

## [1.5.3](https://gitlab.com/ayedocloudsolutions/ake/compare/v1.5.2...v1.5.3) (2021-08-12)


### Bug Fixes

* remove config ([d7b36f1](https://gitlab.com/ayedocloudsolutions/ake/commit/d7b36f113284f6bcb067d035f67211d776248fb5))

## [1.5.2](https://gitlab.com/ayedocloudsolutions/ake/compare/v1.5.1...v1.5.2) (2021-08-12)


### Bug Fixes

* private key file ([774b83e](https://gitlab.com/ayedocloudsolutions/ake/commit/774b83efc61c0c07cbbaaefccf41d5a4a4a1fe51))

## [1.5.1](https://gitlab.com/ayedocloudsolutions/ake/compare/v1.5.0...v1.5.1) (2021-08-12)


### Bug Fixes

* bogus assertion ([10c052c](https://gitlab.com/ayedocloudsolutions/ake/commit/10c052c4cf7de98491a018be1a61e2507b33d9b8))

# [1.5.0](https://gitlab.com/ayedocloudsolutions/ake/compare/v1.4.0...v1.5.0) (2021-08-12)


### Features

* docker ([3269f9a](https://gitlab.com/ayedocloudsolutions/ake/commit/3269f9a135c1d1c9abcd6c431d0679222f89bb6c))

# [1.4.0](https://gitlab.com/ayedocloudsolutions/ake/compare/v1.3.0...v1.4.0) (2021-08-12)


### Features

* docker ([2eb9864](https://gitlab.com/ayedocloudsolutions/ake/commit/2eb98640b15eb76c92354633ce3bd5c562aa6abb))

# [1.3.0](https://gitlab.com/ayedocloudsolutions/ake/compare/v1.2.0...v1.3.0) (2021-08-11)


### Bug Fixes

* stack name Check ([de0fadd](https://gitlab.com/ayedocloudsolutions/ake/commit/de0fadd49f08fed2af0bf8cfab298d62c62b6cd3))


### Features

* added Earthfile ([0f0081e](https://gitlab.com/ayedocloudsolutions/ake/commit/0f0081ed7d39aa5214b62128f63b4dc82fc85ff5))

# [1.2.0](https://gitlab.com/ayedocloudsolutions/ake/compare/v1.1.0...v1.2.0) (2021-08-06)


### Bug Fixes

* variable issue and Pipeline issue ([55fc016](https://gitlab.com/ayedocloudsolutions/ake/commit/55fc0160596409faf1380b47dce253c56b68f7cc))


### Features

* refactored to new acs config structure ([6c92365](https://gitlab.com/ayedocloudsolutions/ake/commit/6c923659f0675b32c5cfb3aa3b6950153661009a))

# [1.1.0](https://gitlab.com/ayedocloudsolutions/ake/compare/v1.0.6...v1.1.0) (2021-06-25)


### Features

* added high-level diagram ([e955b17](https://gitlab.com/ayedocloudsolutions/ake/commit/e955b17ca9f7983805ed25a951e98b3c70a15fe9))

## [1.0.6](https://gitlab.com/ayedocloudsolutions/ake/compare/v1.0.5...v1.0.6) (2021-06-14)


### Bug Fixes

* conditions checking if host is accessible on alternative ssh port ([98b5345](https://gitlab.com/ayedocloudsolutions/ake/commit/98b5345449972231029a44d01b12ccf709351084))
* deleted test config ([5b65e2c](https://gitlab.com/ayedocloudsolutions/ake/commit/5b65e2cc171955e976469cfde675ed173477274d))

## [1.0.5](https://gitlab.com/ayedocloudsolutions/ake/compare/v1.0.4...v1.0.5) (2021-06-13)


### Bug Fixes

* base image ([ef387ec](https://gitlab.com/ayedocloudsolutions/ake/commit/ef387ecc2a1e6669797af4dc25adf7d7d1b85a78))

## [1.0.4](https://gitlab.com/ayedocloudsolutions/ake/compare/v1.0.3...v1.0.4) (2021-06-13)


### Bug Fixes

* CODEOWNERS ([fc918fc](https://gitlab.com/ayedocloudsolutions/ake/commit/fc918fc33510d2e5e7aa7a98c04e01d035b9490a))

## [1.0.3](https://gitlab.com/ayedocloudsolutions/ake/compare/v1.0.2...v1.0.3) (2021-06-13)


### Bug Fixes

* change license to MIT, rename output_dir to release_dir, update README ([ac25aaa](https://gitlab.com/ayedocloudsolutions/ake/commit/ac25aaaac4a29a63bbabdeb4fc2daadda33e4440))

## [1.0.2](https://gitlab.com/ayedocloudsolutions/ake/compare/v1.0.1...v1.0.2) (2021-06-13)


### Bug Fixes

* added multi-master config ([ca2a5c3](https://gitlab.com/ayedocloudsolutions/ake/commit/ca2a5c39db5e72cac57f61f66d4b4615dd1f3763))
* last issues ([22bee28](https://gitlab.com/ayedocloudsolutions/ake/commit/22bee28f12c042ee70258d7b01b64384978c24e2))
* new config option apiserver_endpoint ([8a39abb](https://gitlab.com/ayedocloudsolutions/ake/commit/8a39abbb1c6deee65f90c3e306c90e1abad8c316))
* refactored config for multi-master mode ([51baa4d](https://gitlab.com/ayedocloudsolutions/ake/commit/51baa4d01934164a3246d09c0f4ed47c94b97e50))
* refined multi-master config ([3ea0e22](https://gitlab.com/ayedocloudsolutions/ake/commit/3ea0e22ceba5bf6d1e9e5c61ea6a32da77919448))

## [1.0.1](https://gitlab.com/ayedocloudsolutions/ake/compare/v1.0.0...v1.0.1) (2021-06-03)


### Bug Fixes

* ansible Roles ([a57f23b](https://gitlab.com/ayedocloudsolutions/ake/commit/a57f23beaa735425cfda065d74c58321c9216678))

# 1.0.0 (2021-05-27)


### Features

* Launch ayedo Kubernetes Engine ([5fcc5e3](https://gitlab.com/ayedocloudsolutions/ake/commit/5fcc5e3fce3e7f7f62436eb2edca86ff91e4ba4a))
